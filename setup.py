from setuptools import setup, find_packages


setup(
    name='math-nodes',
    version='0.1.0',
    description='Core math nodes for Kukulkan.',
    author='',
    author_email='',
    license='MIT',
    py_modules=['kukulkan_math_nodes'],
    entry_points={
        'kukulkan.nodes': [
            'Add=kukulkan_math_nodes:Add',
            'Substract=kukulkan_math_nodes:Substract',
            'Multiply=kukulkan_math_nodes:Multiply',
            'Divide=kukulkan_math_nodes:Divide',
        ]
    },
    url='https://gitlab.com/kukulkan/core-packages/math-nodes',
    download_url='git+https://gitlab.com/kukulkan/core-packages/math-nodes.git#egg=math-nodes',
    install_requires=['kukulkan', 'attribute-types'],
    dependency_links=[
        'git+https://gitlab.com/kukulkan/kukulkan#egg=kukulkan',
        'git+https://gitlab.com/kukulkan/core-packages/attribute-types#egg=attribute-types',
    ]
)
