from kukulkan.model.node import Node
import kukulkan.attributes as _attributes
from collections import OrderedDict


class Add(Node):

    attributes = OrderedDict()
    attributes['output'] = {
        'class': _attributes.Float,
        'default_value': 0,
        'plug_type': 'output'
    }
    attributes['input1'] = {
        'class': _attributes.Float,
        'default_value': 0,
        'plug_type': 'input'
    }
    attributes['input2'] = {
        'class': _attributes.Float,
        'default_value': 0,
        'plug_type': 'input'
    }

    def run(self):
        out_value = self.input1 + self.input2
        self.output.set(out_value)


class Substract(Node):

    attributes = OrderedDict()
    attributes['output'] = {
        'class': _attributes.Float,
        'default_value': 0,
        'plug_type': 'output'
    }
    attributes['input1'] = {
        'class': _attributes.Float,
        'default_value': 0,
        'plug_type': 'input'
    }

    attributes['input2'] = {
        'class': _attributes.Float,
        'default_value': 0,
        'plug_type': 'input'
    }

    def run(self):
        out_value = self.input1 - self.input2
        self.output.set(out_value)


class Multiply(Node):

    attributes = OrderedDict()
    attributes['output'] = {
        'class': _attributes.Float,
        'default_value': 0,
        'plug_type': 'output'
    }
    attributes['input1'] = {
        'class': _attributes.Float,
        'default_value': 0,
        'plug_type': 'input'
    }
    attributes['input2'] = {
        'class': _attributes.Float,
        'default_value': 0,
        'plug_type': 'input'
    }

    def run(self):
        out_value = self.input1 * self.input2
        self.output.set(out_value)


class Divide(Node):

    attributes = OrderedDict()
    attributes['output'] = {
        'class': _attributes.Float,
        'default_value': 0,
        'plug_type': 'output'
    }
    attributes['input1'] = {
        'class': _attributes.Float,
        'default_value': 0,
        'plug_type': 'input'
    }
    attributes['input2'] = {
        'class': _attributes.Float,
        'default_value': 0,
        'plug_type': 'input'
    }

    def run(self):
        out_value = self.input1 / self.input2
        self.output.set(out_value)
